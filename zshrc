# ZSH_THEME="bira"
# plugins=(git)

# Aliases
alias mc="/usr/lib/mc/mc-wrapper.sh"
alias mc="env EDITOR=vim PAGER=vim mc --skin darkfar"
alias translate="~/Workspace/utils/translate.py"

alias sci-venv="source /home/tsobiech/.python-venv/sci-venv3.5.2/bin/activate"
alias cd_vs="/home/tsobiech/Workspace/vitalsigns"
