#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
# import urllib

# import goslate

# from bs4 import BeautifulSoup
import re


def get_text():
    p = os.popen('xclip -o', "r")

    lines = p.readlines()
    text = ''
    for line in lines:
        L = line.strip()
        if L.endswith('-'):
            text += L[:-1]
        else:
            text += L
            text += ' '

    return text


if __name__ == '__main__':
    text = '"' + get_text() + '"'
    p = os.popen('trans -l pl -s en -t pl ' + text, "r")
    out_text = "".join(p.readlines())
    out_text = "".join(re.split("\x1b\[[0-9;]*[a-zA-Z]", out_text))
    print(out_text)
    query = u'notify-send "%s"' % out_text
    os.system(query.encode("utf-8"))

# trans -l pl -s en -t pl
# # print line
# queries = []
# try:
#     gs = goslate.Goslate()
#     queries.append(u'notify-send "' + gs.translate(line, 'pl') + u'"')

# except Exception as e:
#     # Nevertheless
#     # params = urllib.urlencode({'spam': 1, 'eggs': 2, 'bacon': 0})
#     try:
#         line = line.split(" ")[0]

#         url = "http://pl.bab.la/slownik/angielski-polski/" + line
#         f = urllib.urlopen(url)
#         soup = BeautifulSoup(f.read(), 'html.parser')
#         div = soup.find_all('div')
#         div = filter(lambda x: x.get('class', ['', ])[
#                      0] == "result-wrapper", div)
#         # print(div)
#         words = []
#         for d in div:
#             a = d.find_all('a')
#             a = filter(lambda x: x.get('class', ['', ])[0] == "result-link", a)
#             words.append(a)
#         N = len(words) if len(words) < 5 else 5
#         # print(N)
#         for i in range(N):
#             queries.append(u'notify-send "' + words[i][1].text + u'"')
#         # query = u'notify-send "HTTP Error 503: Service Unavailable"'
#         # raise e

#     except Exception as e:
#         queries.append(u'notify-send "NIe udało się :-( "')

# for query in queries:
#     os.system(query.encode("utf-8"))
